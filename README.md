# email-signature

This is an email signature. The goal is to create a beautiful and simple
signature that clearly identifies the author of the email.

New themes are being thought of.

## Setup
This project is powered by [Jekyll](https://jekyllrb.com/docs/quickstart/), a
static site generator.

Requirements:
* [Ruby](https://www.ruby-lang.org/en/downloads/)
* [RubyGems](https://rubygems.org/pages/download)

Getting the project running:
1. `gem install jekyll`
2. `cd <location of email-signature repo>`
3. `jekyll serve`
4. Navigate to http://localhost:4000
